import { useState } from "react";
import { AppContext } from "../common/context/Context";
import "./app.scss";
import { Header } from "./components/header/Header";
import { Route, Routes, Link, useLocation } from "react-router-dom";
import { Modal } from "../common/components/modal/Modal";
import { Films } from "./components/main/films/Films";
import { CentralBlock } from "./components/main/central-block/CentralBlock";
import { Posts } from "./components/main/posts/Posts";
import { QuestionBlock } from "./components/main/question-block/QuestionBlock";
import { Injection } from "../common/components/injection/injection";
import arrowRight from "../assets/images/icons/arrow-right.svg";
import { Footer } from "./components/footer/Footer";

const selectImageElement = () => {
  const elem = document.createElement("img");
  return elem;
};

export const App = () => {
  const [lang, setLang] = useState("English");
  const location = useLocation();

  return (
    <AppContext.Provider value={{ lang, setLang }}>
      <div className="app">
        <div className="container">
          {/* <img className="app__main_image" src={main_image} alt="main_image" /> */}
          <div
            className={
              location.pathname === "/sign"
                ? "app__top_content signin"
                : "app__top_content"
            }
          >
            {/* <Films/> */}
            <Routes>
              <Route
                path="/sign"
                element={
                  <>
                    <Header />
                    <Modal
                      footerContent={true}
                      clName="app__signin_modal"
                      buttonText="Sign In"
                      bottomText={true}
                    />
                  </>
                }
              />
              <Route
                path="/"
                element={
                  <>
                    <Header />
                    <CentralBlock />
                  </>
                }
              />
            </Routes>
          </div>
          <Routes>
            <Route
              path="/"
              element={
                <>
                  <Posts />
                  <QuestionBlock />
                  <Footer/>
                </>
              }
            />
          </Routes>
        </div>
        {/* <Injection
          parentQuerySelector="app__signin_modal"
          className="app__signin_modal_parent"
          el="div"
        >
          <div>
            <p className="app__signin_modal__bottom_text">
              <a>Remember me</a>
              <a>Need help?</a>
            </p>
          </div>
        </Injection> */}
      </div>
    </AppContext.Provider>
  );
};
