import { EmailBlock } from "../email-block/EmailBlock";
import { QuestionBlockItem } from "./components/QuestionBlockItem";
import { questions } from "./components/questions";
import "./question-block.scss";

export const QuestionBlock = () => {
  return (
    <div className="question_block">
      <h3 className="question_block__title">Frequently Asked Questions</h3>
      {questions.map((item, i) => (
        <QuestionBlockItem
          key={item + i}
          text={item}
          active_text={`Watch Netflix on your smartphone, tablet, Smart TV, laptop, 
          or streaming device, all for one fixed monthly fee. Plans range 
          from ₹ 199 to ₹ 799 a month. No extra costs, no contracts.`}
        />
      ))}
      <EmailBlock />
    </div>
  );
};
