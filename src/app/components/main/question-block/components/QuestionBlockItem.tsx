import { useState } from "react";
import plus from "../../../../../assets/images/icons/plus.svg";
import close from "../../../../../assets/images/icons/close.svg";
import "./question-block-item.scss";

interface QuestionBlockItemProps {
  text?: string;
  active_text?: string;
}

export const QuestionBlockItem = ({
  text,
  active_text,
}: QuestionBlockItemProps) => {
  const [active, setActive] = useState(false);
  return (
    <div className="question_block_item">
      <div className="question_block_item__button">
        <strong className="question_block_item__text">{text}</strong>
        <div
          onClick={() => setActive(!active)}
          className={active ? "question_block_item__image question_block_item__image__active" : "question_block_item__image"}
        />
      </div>
      <div className={active ? "question_block_item__dropdown_text question_block_item__dropdown_text__inactive" : "question_block_item__dropdown_text"}>
        {active_text}
      </div>
    </div>
  );
};
