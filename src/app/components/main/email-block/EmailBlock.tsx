import { Button } from "../../../../common/components/buttons/button";
import arrowRight from "../../../../assets/images/icons/arrow-right.svg";
import "./email-block.scss";
import { Injection } from "../../../../common/components/injection/injection";

export const EmailBlock = () => {
  return (
    <>
      <p className="email_title">
        Ready to watch? Enter your email to create or restart your membership.
      </p>
      <span className="email_block">
        <input
          type="email"
          placeholder="Email adress"
          className="email_block__input"
        />
        <Button
          clName="email_block__button"
          text="Get Started"
          src={arrowRight}
          clNameImage="email_block__image"
        />
        <Injection
          parentQuerySelector="email_block__button"
          className="email_block__image_parent"
          el="div"
        >
          <div>
            <img
              src={arrowRight}
              alt="arrow-right"
              className="email_block__image"
            />
          </div>
        </Injection>
      </span>
    </>
  );
};
