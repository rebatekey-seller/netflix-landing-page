import { useQuery } from "@apollo/client";
import { GET_ALL_FILMS } from "../../../../common/services/query/films";
import { FilmsList } from "../../../../common/services/types";

export const Films = () => {
  const { data, loading, error } = useQuery<FilmsList>(GET_ALL_FILMS);

  console.log(data)

  if (loading) {
      return <div>...Loading</div>
  }

  if (error) {
      return <div>Error</div>
  }

  if (!data) return null;

  return (
    <div>
      {data.data.allFilms.films.map((item, i) => (
        <h3 key={item.title + i}>{item.title}</h3>
      ))}
    </div>
  );
};
