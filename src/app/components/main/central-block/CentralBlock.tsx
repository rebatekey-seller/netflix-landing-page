import "./central-block.scss";
import { Button } from "../../../../common/components/buttons/button";
import { EmailBlock } from "../email-block/EmailBlock";

export const CentralBlock = () => {
  return (
    <div className="central_block">
      <h1 className="central_block__title">
        Unlimited movies, TV shows and more.
      </h1>
      <h3 className="central_block__subtitle">
        Watch anywhere. Cancel anytime.
      </h3>
      <div className="central_block__enter_email">
        <EmailBlock />
      </div>
    </div>
  );
};
