import { PostItem } from "./components/PostItem";
import tv from "../../../../assets/images/tv.svg";
import save from "../../../../assets/images/save.svg";
import stream from "../../../../assets/images/stream.svg";
import kids from "../../../../assets/images/kids.svg";
import "./posts.scss";

export const Posts = () => {
  return (
    <div className="posts">
      <PostItem
        title="Enjoy on your TV."
        subtitle="Watch on Smart TVs, Playstation, Xbox, Chromecast, Apple TV, Blu-ray players, and more."
        image={tv}
      />
      <PostItem
        title="Download your shows to watch offline."
        subtitle="Save your favorites easily and always have something to watch."
        image={save}
      />
      <PostItem
        title="Watch everywhere."
        subtitle="Stream unlimited movies and TV shows on your phone, tablet, laptop, and TV without paying more."
        image={stream}
      />
      <PostItem
        title="Create profiles for kids."
        subtitle="Send kids on adventures with their favorite characters in a space made just for them—free with your membership."
        image={kids}
      />
    </div>
  );
};
