import "./post-item.scss";

interface PostItemProps {
  title?: string;
  subtitle?: string;
  image?: string;
}

export const PostItem = ({ title, subtitle, image }: PostItemProps) => {
  return (
    <div className="post_item">
      <div className="post_item__content">
        <div className="post_item__text">
          <h3 className="post_item__title">{title}</h3>
          <p className="post_item__subtitle">{subtitle}</p>
        </div>
        <div>
          <img src={image} className="post_item__image" />
        </div>
      </div>
    </div>
  );
};
