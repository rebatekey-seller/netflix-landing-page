import logo from "assets/images/logo.svg";
import "./header.scss";
import { Link, useLocation } from "react-router-dom";
import { LanguagesDropdown } from "../../../common/components/languages-dropdown/LanguagesDropdown";
import globe from "../../../assets/images/icons/globe.svg";
import arrow from "../../../assets/images/icons/arrow.svg";

export const Header = () => {
  const location = useLocation();

  return (
    <header className="header">
      <img src={logo} alt="logo" className="header__logo" />
      {location.pathname === "/" && (
        <div className="header__items">
          <LanguagesDropdown src={globe} srcArrow={arrow} />
          <Link to="/sign" className="header__button">
            Sign In
          </Link>
        </div>
      )}
    </header>
  );
};
