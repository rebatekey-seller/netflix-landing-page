import { LanguagesDropdown } from "../../../common/components/languages-dropdown/LanguagesDropdown";
import globeDark from "../../../assets/images/icons/globe-dark.svg";
import arrowDark from "../../../assets/images/icons/arrow-dark.svg";
import "./footer.scss";

export const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <h3 className="footer__title">Questions? Call 1-844-505-2993</h3>
        <div className="footer__links">
          <div className="footer__links_item">
            <a>FAQ</a>
            <a>Investor Relations</a>
            <a>Ways to Watch</a>
            <a>Corporate Information</a>
            <a>Only on Netflix</a>
          </div>
          <div className="footer__links_item">
            <a>Help Center</a>
            <a>Jobs</a>
            <a>Terms of Use</a>
            <a>Contact Us</a>
          </div>
          <div className="footer__links_item">
            <a>Account</a>
            <a>Redeem Gift Cards</a>
            <a>Privacy</a>
            <a>Speed Test</a>
          </div>
          <div className="footer__links_item">
            <a>Media Center</a>
            <a>Buy Gift Cards</a>
            <a>Cookie Preferences</a>
            <a>Legal Notices</a>
          </div>
        </div>
        <LanguagesDropdown clName="footer_languages" src={globeDark} srcArrow={arrowDark} />
        <p className="footer__bottom_text">Netflix India</p>
      </div>
    </div>
  );
};
