import "./footer-content.scss";

interface FooterContentProps {
  facebookText?: string;
  facebookIcon?: string;
  clName?: string;
}

export const FooterContent = ({
  facebookText,
  facebookIcon,
  clName,
}: FooterContentProps) => {
  return (
    <div className={clName + "__block"}>
      {facebookText && facebookIcon && (
        <div className={clName + "__facebook_link"}>
          <img
            className={clName + "__facebook_icon"}
            src={facebookIcon}
            alt="facebook"
          />
          {facebookText}
        </div>
      )}
      <span className={clName + "__sign_up_now"}>
        New to Netflix? <strong>Sign up now.</strong>
      </span>
      <p className={clName + "__main_text"}>
        This page is protected by Google reCAPTCHA to ensure you're not a bot.
        <a>Learn more.</a>
      </p>
    </div>
  );
};
