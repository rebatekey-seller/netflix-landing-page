import { useLayoutEffect, useRef } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Button } from "../buttons/button";
import { FooterContent } from "./components/footer-content/footerContent";
import facebookIcon from "../../../assets/images/icons/facebook.svg";

interface ModalProps {
  clName?: string;
  bottomText: boolean;
  title?: string;
  buttonText?: string;
  footerContent: boolean;
}

export const Modal = ({
  clName,
  bottomText,
  buttonText,
  title,
  footerContent,
}: ModalProps) => {
  const ref = useRef<HTMLDivElement>(null);
  const navigate = useNavigate();

  return (
    <div ref={ref} className={clName}>
      <div className={clName + "__close"} onClick={() => navigate(-1)}>
        Back
      </div>
      <h3>{title}</h3>
      <input
        className={clName + "__input"}
        type="email"
        placeholder="Email or phone number"
      />
      <input
        className={clName + "__input"}
        type="password"
        placeholder="Password"
      />
      <Button text={buttonText} clName={clName + "__button"} />
      {bottomText && (
        <p className="app__signin_modal__bottom_text">
          <a>Remember me</a>
          <a>Need help?</a>
        </p>
      )}
      {footerContent && <FooterContent clName="modal_footer" facebookIcon={facebookIcon} facebookText="Login with Facebook" />}
    </div>
  );
};
