import { useEffect, useLayoutEffect, useRef } from "react";
import "./button.scss";

interface ButtonProps {
  text?: string;
  clName?: string;
  buttonFunc?: () => void;
  clNameImage?: string;
  src?: string;
}

export const Button = ({
  text,
  clName,
  buttonFunc,
  clNameImage,
  src,
}: ButtonProps) => {
  return (
    <button
      className={"button " + clName}
      onClick={() => (buttonFunc ? buttonFunc() : false)}
    >
      {text ? text : "text"}
      {src && <img src={src} alt="button_icon" className={clNameImage} />}
    </button>
  );
};
