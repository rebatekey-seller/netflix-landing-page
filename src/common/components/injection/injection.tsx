import * as React from "react";
import ReactDOM from "react-dom";

export interface InjectionProps {
  el: keyof HTMLElementTagNameMap;
  className?: string;
  parentQuerySelector: string;
  children: React.ReactNode;
}

export const Injection: React.FC<InjectionProps> = ({
  children,
  className = "root-portal",
  el = "div",
  parentQuerySelector
}) => {
  const [container] = React.useState(() => {
    return document.createElement(el);
  });

  const parentElement = document.querySelector(parentQuerySelector);

  React.useLayoutEffect(() => {
    container.classList.add(className);
    if (parentElement) {
        parentElement.appendChild(container);
        return () => {
            parentElement.removeChild(container);
        };
    }
  }, []);


  return ReactDOM.createPortal(children, container);
};
