import { useContext } from "react";
import { AppContext } from "../../context/Context";
import "./languages-dropdown.scss";

interface LanguagesDropdownProps {
  clName?: string;
  src?: string;
  srcArrow?: string;
}

export const LanguagesDropdown = ({ clName, src, srcArrow }: LanguagesDropdownProps) => {
  const context = useContext(AppContext);

  const toggleClick = (e: React.MouseEvent<HTMLElement>) => {
    const target = e.currentTarget;
    target.classList.toggle("languages__select__active");
    Array.from(target.children).forEach((item) => {
      if (item.tagName === "UL") {
        item.classList.toggle("languages__dropdown_active");
        Array.from(item.children).forEach((item) => {
          if (item.tagName === "LI") {
            item.addEventListener("click", () => {
              context.setLang(item.textContent);
            });
          }
        });
      }
    });
  };
  return (
    <div className={`languages ${clName}`}>
      <div className="languages__item">
        <div className="languages__select" onClick={(e) => toggleClick(e)}>
          {src && (<img src={src} alt="globw" className="languages__logo" />)}
          <strong>
            {context.lang}
            {srcArrow && (<img src={srcArrow} alt="arrow" className="languages__arrow" />)}
          </strong>
          <ul>
            <li>English</li>
            <li>Russian</li>
          </ul>
        </div>
      </div>
    </div>
  );
};
