export interface FilmsList {
  data: {
    allFilms: {
      films: [{ title: string }];
    };
  };
}
